# Cheesemaker

Downstream extension modules' wheel builder for the [floating cheeses]

## Contributing

Generally, don't.  This repository has the sole purpose of triggering GitLab CI.
If you want to use it as a template to build other wheels, clone it and push
under a namespace you manage.  If you found something wrong and want to discuss,
send your thoughts to one of the [mailing lists for the floating cheeses][mail].

## Copying

This is free and unencumbered software released into the public domain.

[floating cheeses]: https://man.sr.ht/~cnx/ipwhl
[mail]: https://man.sr.ht/~cnx/ipwhl/#mailing-lists
